# Gateway_Docking_Station
The project is based on the design of a gateway that provides a remote network with connectivity to a host network.

# Getting Started
- Make sure that you have a C1_Node_revb_v1.0.
- Raspberry Pi 3B+.
- RAK 833.


# Prerequisites
Board [Tested]

# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Acknowledgments

# Changelog
